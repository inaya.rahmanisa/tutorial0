package id.ac.ui.cs.advprog.tutorialadpro.service;

import id.ac.ui.cs.advprog.tutorialadpro.model.Activity;
import id.ac.ui.cs.advprog.tutorialadpro.model.Day;
//
import java.util.List;

public interface ActivityService {
    public Activity create(Activity activity);
    public List<Activity> findAll();
    public List<Activity> findByDay(Day day);

}
