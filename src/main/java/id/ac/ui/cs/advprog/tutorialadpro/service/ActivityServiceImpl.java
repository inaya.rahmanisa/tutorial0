package id.ac.ui.cs.advprog.tutorialadpro.service;

import id.ac.ui.cs.advprog.tutorialadpro.model.Activity;
import id.ac.ui.cs.advprog.tutorialadpro.model.Day;
import id.ac.ui.cs.advprog.tutorialadpro.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public Activity create(Activity activity) {
        activityRepository.create(activity);
        return activity;
    }

    @Override
    public List<Activity> findAll() {
        Iterator<Activity> activityIterator = activityRepository.findAll();
        List<Activity> allActivity = new ArrayList<Activity>();
        activityIterator.forEachRemaining(allActivity::add);
        return allActivity;
    }

    @Override
    public List<Activity> findByDay(Day day) {
        // TO DO: get a list of activities that match the day
        List<Activity> activities = new ArrayList<>();
        Iterator<Activity> activityIterator = activityRepository.findAll();
        while(activityIterator.hasNext())   {
            Activity activity = activityIterator.next();
            if (activity.getDay().equals(day))   {
                activities.add(activity);
            }
        }

//        for (Activity k : activities)    {
//            System.out.println(k.getName());
//        }
        return activities;
    }


}
