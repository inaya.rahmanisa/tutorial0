package id.ac.ui.cs.advprog.tutorialadpro.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Activity {
    private String name;
    private Day day;
}
